import axios from "axios";
import { Grade, GradeIncomingDto } from "../types/gradeType";
import { Student, StudentIncomingDto } from "../types/studentType";

export const getStudents = async () : Promise<Student[]> => {
    return await axios
        .get(`http://localhost:8080/api/students`)
        .then((res) => {
            if (res.status == 200) {
                return res.data;
            }
        })
        .catch((error) => {
          throw new Error(error.message);
        });
    };

export const getStudentById = async (studentId: number) : Promise<Student> => {
        return await axios
            .get(`http://localhost:8080/api/students/${studentId}`)
            .then((res) => {
                if (res.status == 200) {
                    return res.data;
                }
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        };

export const getGradesByStudentId = async (studentId: number) : Promise<Grade[]> => {
    return await axios
        .get(`http://localhost:8080/api/students/${studentId}/grades`)
        .then((res) => {
            if (res.status == 200) {
                return res.data;
            }
        })
        .catch((error) => {
          throw new Error(error.message);
        });
    };

export const addGradesByStudentId = async (studentId: number, gradeIncomingDto: GradeIncomingDto) : Promise<Grade> => {
        return await axios
            .post(`http://localhost:8080/api/students/${studentId}/grades`,
            gradeIncomingDto)
            .then((res) => {
                if (res.status == 200) {
                    return res.data;
                }
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        };

export const addStudent = async ( studentIncomingDto: StudentIncomingDto) : Promise<Student> => {
        return await axios
                .post(
                  "http://localhost:8080/api/students",
                  studentIncomingDto)
                .then((res) => {
                  return res.data;
                })
                .catch((error) => {
                  throw new Error(error.message);
                });
        };

export const deleteStudent = async (studentId: number) : Promise<void> => {
            return await axios
                .delete(
                  `http://localhost:8080/api/students/${studentId}`)
                .then((res) => {
                  return res.data;
                })
                .catch((error) => {
                  throw new Error(error.message);
                });
        };

export const updateStudent = async (studentId: number, student: Student) : Promise<Student> => {
          return await axios
              .put(
                `http://localhost:8080/api/students/${studentId}`,
                student)
              .then((res) => {
                return res.data;
              })
              .catch((error) => {
                throw new Error(error.message);
              });
      };