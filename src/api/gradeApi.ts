import axios from "axios";
import { Grade, GradeIncomingDto } from "../types/gradeType";

export const getGrades = async () : Promise<Grade[]> => {
    return await axios
        .get(`http://localhost:8080/api/grades`)
        .then((res) => {
            if (res.status == 200) {
                return res.data;
            }
        })
        .catch((error) => {
          throw new Error(error.message);
        });
    };

export const getGradeById = async (gradeId: number) : Promise<Grade> => {
        return await axios
            .get(`http://localhost:8080/api/grades/${gradeId}`)
            .then((res) => {
                if (res.status == 200) {
                    return res.data;
                }
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        };

export const getGradesBySubject = async (subject: string) : Promise<Grade[]> => {
          return await axios
              .get(`http://localhost:8080/api/grades?subject=${subject}`)
              .then((res) => {
                  if (res.status == 200) {
                      return res.data;
                  }
              })
              .catch((error) => {
                throw new Error(error.message);
              });
          };

export const addGrade = async ( gradeIncomingDto: GradeIncomingDto) : Promise<Grade> => {
    return await axios
            .post(
              "http://localhost:8080/api/grades",
              gradeIncomingDto)
            .then((res) => {
              return res.data;
            })
            .catch((error) => {
              throw new Error(error.message);
            });
    };

export const deleteGrade = async (gradeId: number) : Promise<void> => {
        return await axios
            .delete(
              `http://localhost:8080/api/grades/${gradeId}`)
            .then((res) => {
              return res.data;
            })
            .catch((error) => {
              throw new Error(error.message);
            });
    };

export const updateGrade = async (gradeId: number, grade: Grade) : Promise<Grade> => {
      return await axios
          .put(
            `http://localhost:8080/api/grades/${gradeId}`,
            grade)
          .then((res) => {
            return res.data;
          })
          .catch((error) => {
            throw new Error(error.message);
          });
  };
