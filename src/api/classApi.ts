import axios from "axios";
import { Class, ClassIncomingDto } from "../types/classTypes";
import { Student, StudentIncomingDto } from "../types/studentType";

export const getClasses = async () : Promise<Class[]> => {
    return await axios
        .get(`http://localhost:8080/api/classes`)
        .then((res) => {
            return res.data;
        })
        .catch((error) => {
          throw new Error(error.message);
        });
    };

export const getClassById = async (classId: number) : Promise<Class> => {
        return await axios
            .get(`http://localhost:8080/api/classes/${classId}`)
            .then((res) => {
                return res.data;
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        };

export const getStudentsByClassId = async (classId: number) : Promise<Student[]> => {
        return await axios
            .get(`http://localhost:8080/api/classes/${classId}/students`)
            .then((res) => {
                if (res.status == 200) {
                    return res.data;
                }
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        };

export const addStudentsByClassId = async (classId: number, studentIncomingDto: StudentIncomingDto) : Promise<Student> => {
    return await axios
                .post(`http://localhost:8080/api/classes/${classId}/students`,
                studentIncomingDto)
                .then((res) => {
                    if (res.status == 200) {
                        return res.data;
                    }
                })
                .catch((error) => {
                  throw new Error(error.message);
                });
            };

export const addClass = async ( classIncomingDto: ClassIncomingDto) : Promise<Class> => {
    return await axios
        .post(
          "http://localhost:8080/api/classes",
          classIncomingDto)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error.message);
        });
};

export const deleteClass = async (classId: number) : Promise<void> => {
    return await axios
        .delete(
          `http://localhost:8080/api/classes/${classId}`)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error.message);
        });
};

export const updateClass = async (classId: number, schoolClass: Class) : Promise<Class> => {
    return await axios
        .put(
          `http://localhost:8080/api/classes/${classId}`,
          schoolClass)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error.message);
        });
};