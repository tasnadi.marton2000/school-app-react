import { Button } from "@mui/material";
import { Link } from "react-router-dom";

interface MenuButtonProps {
  uri: string;
  text: string;
}

function MenuButtons(props: MenuButtonProps) {
  return (
    <Button
      component={Link}
      to={props.uri}
      color="inherit"
      sx={{ marginLeft: "10px" }}
    >
      {props.text}
    </Button>
  );
}

export default MenuButtons;
