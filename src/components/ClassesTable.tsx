import { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  CssBaseline,
  Box,
  Typography,
  Button,
} from "@mui/material";
import MenuBar from "./Menu";
import { Delete, Info } from "@mui/icons-material";
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from "react-router-dom";
import { deleteClass, getClasses } from "../api/classApi";
import { Class } from "../types/classTypes";

function ClassesTable() {
  const navigate = useNavigate();
  const [classes, setClasses] = useState<Class[]>([]);

  const getClassesData = async () => {
    try {
      const classesList: Class[] = await getClasses();
      setClasses(classesList);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getClassesData();
  }, []);

  const addClass = () => {
    navigate("/addClass")
  };

  return (
    <div>
      <Box
        sx={{
          borderRadius: 1,
          boxShadow: 5,
          marginTop: 8,
          marginBotton: 8,
          p: 2,
        }}
      >
        <CssBaseline />
        <MenuBar />
        <Typography variant="h6" align="center" gutterBottom>
          Classes
        </Typography>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
              <TableCell>ClassId</TableCell>
                <TableCell>Class</TableCell>
                <TableCell>Abbreviation</TableCell>
                <TableCell>Profile</TableCell>
                <TableCell align="center">Details</TableCell>
                <TableCell align="center">Update</TableCell>
                <TableCell align="center">Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {classes.map((schoolClass: Class) => (
                <TableRow key={schoolClass.id}>
                  <TableCell>{schoolClass.id}</TableCell>
                  <TableCell>{schoolClass.nrClass}</TableCell>
                  <TableCell>{schoolClass.abbreviation}</TableCell>
                  <TableCell>{schoolClass.profile}</TableCell>
                  <TableCell align="center">
                    <IconButton
                      onClick={() => {
                        navigate(`/classes/${schoolClass.id}/students`);
                      }}
                    >
                      <Info />
                    </IconButton>
                  </TableCell>
                  <TableCell align="center">
                    <IconButton onClick = {() => {
                        navigate(`/classes/${schoolClass.id}/update`);
                      }}>
                      <EditIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell align="center">
                    <IconButton onClick = {() => {
                        deleteClass(schoolClass.id);
                        window.location.reload();
                      }}>
                      <Delete />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={addClass}
          sx={{ marginBottom: 2,
                marginTop: 2,
                width: "50%",
            }}
        >
          Add Class
        </Button>
      </Box>
    </div>
  );
}

export default ClassesTable;
