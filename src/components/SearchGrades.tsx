import { TextField, Typography, Button, Box, CssBaseline, FormControl } from "@mui/material";
import { ChangeEvent, useState } from "react";
import Menu from "./Menu";
import { useNavigate } from "react-router-dom";

function SearchGrades() {
  const navigate = useNavigate();
  const [subject, setSubject] = useState("");

  const handleSearch = () => {
    if (subject == "") {
        navigate('/grades');
    } else {
        navigate(`/grades?subject=${subject}`)
    }
  };

  const handleChangeSubject = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setSubject(event.target.value);
  }

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Search Grade By Subject</Typography>
        <FormControl>
            <TextField
            id="subject-textField"
            label="Grade"
            value={subject}
            InputLabelProps={{
                shrink: true,
            }}
            onChange={handleChangeSubject}
            sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
            />
        </FormControl>
        <Button variant="contained" onClick={handleSearch}>
          Search
        </Button>
      </Box>
    </div>
  );
}

export default SearchGrades;
