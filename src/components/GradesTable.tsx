import { ChangeEvent, useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  CssBaseline,
  Box,
  Typography,
  Button,
  FormControl,
  TextField,
} from "@mui/material";
import MenuBar from "./Menu";
import { Delete } from "@mui/icons-material";
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { Grade } from "../types/gradeType";
import { deleteGrade, getGrades, getGradesBySubject } from "../api/gradeApi";
import { getGradesByStudentId } from "../api/studentApi";

function GradesTable() {
  const navigate = useNavigate();
  const [grades, setGrades] = useState<Grade[]>([]);
  const { studentId } = useParams();
  const [searchParams, setSearchParams] = useSearchParams();

  const getGradesData = async () => {
    const subject = searchParams.get("subject");
    try {
      let gradesList: Grade[];
      if (studentId == null && subject == null) {
        gradesList = await getGrades();
      } else if (studentId == null && subject != null) {
        gradesList = await getGradesBySubject(subject);
      } else {
        gradesList = await getGradesByStudentId(parseInt(studentId));
      }
      setGrades(gradesList);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getGradesData();
  }, []);

  const addGrade = () => {
    if (studentId == null) {
      navigate("/addGrade");
    } else {
      navigate(`/students/${studentId}/addGrade`);
    }
  };

  const searchGrade = () => {
    navigate('/search');
  }

  return (
    <div>
      <Box
        sx={{
          borderRadius: 1,
          boxShadow: 5,
          marginTop: 8,
          marginBotton: 8,
          p: 2,
        }}
      >
        <CssBaseline />
        <MenuBar />
        <Typography variant="h6" align="center" gutterBottom>
          Grades
        </Typography>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Grade</TableCell>
                <TableCell>Note</TableCell>
                <TableCell>Subject</TableCell>
                <TableCell>StudentId</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Type</TableCell>
                <TableCell align="center">Update</TableCell>
                <TableCell align="center">Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {grades.map((grade: Grade) => (
                <TableRow key={grade.id}>
                  <TableCell>{grade.grade}</TableCell>
                  <TableCell>{grade.notes}</TableCell>
                  <TableCell>{grade.subject}</TableCell>
                  <TableCell>{grade.studentId}</TableCell>
                  <TableCell>{grade.date.toString()}</TableCell>
                  <TableCell>{grade.type}</TableCell>
                  <TableCell align="center">
                    <IconButton onClick = {() => {
                        navigate(`/grades/${grade.id}/update`);
                      }}>
                      <EditIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell align="center">
                    <IconButton onClick = {() => {
                        deleteGrade(grade.id);
                        window.location.reload();
                      }}>
                      <Delete />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={addGrade}
          sx={{ marginBottom: 2,
                marginTop: 2,
                width: "50%",
            }}
        >
          Add Grade
        </Button>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={searchGrade}
          sx={{ marginBottom: 2,
                marginTop: 2,
                width: "50%",
            }}
        >
          Search Grade
        </Button>
      </Box>
    </div>
  );
}

export default GradesTable;
