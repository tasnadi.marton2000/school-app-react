import { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  CssBaseline,
  Box,
  Typography,
  Button,
  FormControl,
} from "@mui/material";
import MenuBar from "./Menu";
import { Delete, Info } from "@mui/icons-material";
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate, useParams } from "react-router-dom";
import { Student } from "../types/studentType";
import { deleteStudent, getStudents } from "../api/studentApi";
import { getStudentsByClassId } from "../api/classApi";

function ClassesTable() {
  const navigate = useNavigate();
  const { classId } = useParams();
  const [students, setStudents] = useState<Student[]>([]);

  const getStudentsData = async () => {
    try {
      let studentList: Student[];
      if (classId == null) {
        studentList = await getStudents();
      } else {
        studentList = await getStudentsByClassId(parseInt(classId));
      }
      setStudents(studentList);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getStudentsData();
  }, []);

  const addStudent = () => {
    console.log(students);
    if (classId == null) {
      navigate("/addStudent");
    } else {
      navigate(`/classes/${classId}/addStudent`);
    }
  };

  return (
    <div>
      <Box
        sx={{
          borderRadius: 1,
          boxShadow: 5,
          marginTop: 8,
          marginBotton: 8,
          p: 2,
        }}
      >
        <CssBaseline />
        <MenuBar />
        <Typography variant="h6" align="center" gutterBottom>
          Students
        </Typography>
        <FormControl>

        </FormControl>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>ClassId</TableCell>
                <TableCell align="center">Details</TableCell>
                <TableCell align="center">Update</TableCell>
                <TableCell align="center">Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {students.map((student: Student) => (
                <TableRow key={student.id}>
                  <TableCell>{student.name}</TableCell>
                  <TableCell>{student.classId}</TableCell>
                  <TableCell align="center">
                    <IconButton
                      onClick={() => {
                        navigate(`/students/${student.id}/grades`);
                      }}
                    >
                      <Info />
                    </IconButton>
                  </TableCell>
                  <TableCell align="center">
                    <IconButton onClick={() => {
                          navigate(`/students/${student.id}/update`);
                        }}>
                      <EditIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell align="center">
                    <IconButton onClick = {() => {
                        deleteStudent(student.id);
                        window.location.reload();
                      }}>
                      <Delete />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={addStudent}
          sx={{ marginBottom: 2,
                marginTop: 2,
                width: "50%",
            }}
        >
          Add Student
        </Button>
      </Box>
    </div>
  );
}

export default ClassesTable;
