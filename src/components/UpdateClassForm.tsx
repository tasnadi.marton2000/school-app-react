import { TextField, Typography, Button, Box, CssBaseline } from "@mui/material";
import { ChangeEvent, useEffect, useState } from "react";
import Menu from "./Menu";
import { Class } from "../types/classTypes";
import { getClassById, updateClass } from "../api/classApi";
import { useNavigate, useParams } from "react-router-dom";

function UpdateClassForm() {
  const { classId } = useParams();
  const navigate = useNavigate();
  const [nrClass, setNrClass] = useState(0);
  const [abbreviation, setAbbreviation] = useState("");
  const [profile, setProfile] = useState("");

  const setInitialData = async () => {
    if (classId != undefined) {
        const response: Class = await getClassById(parseInt(classId));
        setNrClass(response.nrClass);
        setAbbreviation(response.abbreviation);
        setProfile(response.profile);
    }
  };

  useEffect(() => {
    setInitialData();
  }, []);

  const handleChangeAbbreviation = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setAbbreviation(event.target.value);
  };

  const handleChangeProfile = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setProfile(event.target.value);
  };

  const handleChangeNrClass = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setNrClass(parseInt(event.target.value));
  };

  const handleUpdateClass = async () => {
    if (classId != undefined) {
        const schoolClass: Class = {
            id: parseInt(classId),
            nrClass: nrClass,
            abbreviation: abbreviation,
            profile: profile,
        };

        const response = await updateClass(parseInt(classId), schoolClass);
    }

    navigate('/classes');
  }

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Update Class</Typography>
        <TextField
          id="class-nrClass-textField"
          label="Class"
          variant="outlined"
          type="number"
          onChange={handleChangeNrClass}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="class-abbreviation-textField"
          label="Abbreviation"
          variant="outlined"
          onChange={handleChangeAbbreviation}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="class-profile-textField"
          label="Profile"
          variant="outlined"
          onChange={handleChangeProfile}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <Button variant="contained" onClick={handleUpdateClass}>
          Add Class
        </Button>
      </Box>
    </div>
  );
}

export default UpdateClassForm;
