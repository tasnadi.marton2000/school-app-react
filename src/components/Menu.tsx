import { AppBar, Toolbar } from "@mui/material";
import MenuButtons from "./MenuButtons";

function Menu() {

  return (
    <AppBar>
      <Toolbar
        sx={{
          display: "flex",
          flexDirection: "row-reverse",
        }}
      >
        <MenuButtons uri="/grades" text="Grades" />
        <MenuButtons uri="/students" text="Students" />
        <MenuButtons uri="/classes" text="Classes" />
        <MenuButtons uri="/" text="Home" />
      </Toolbar>
    </AppBar>
  );
}

export default Menu;
