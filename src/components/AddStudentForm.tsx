import { TextField, Typography, Button, Box, CssBaseline } from "@mui/material";
import { ChangeEvent, useState } from "react";
import Menu from "./Menu";
import { useNavigate, useParams } from "react-router-dom";
import { addStudent } from "../api/studentApi";
import { StudentIncomingDto } from "../types/studentType";
import { addStudentsByClassId } from "../api/classApi";

function AddCurrency() {
  const navigate = useNavigate();
  const { classId } = useParams();
  const [name, setName] = useState("");
  const [schoolClassId, setSchoolClassId] = useState(0);

  const handleChangeName = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setName(event.target.value);
  };

  const handleChangeClassId = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setSchoolClassId(parseInt(event.target.value));
  };

  const handleAddStudent = async () => {
    let response;
    if (classId == undefined) {
        const studentIncomingDto: StudentIncomingDto = {
            name: name,
            classId: schoolClassId,
        };
        response = await addStudent(studentIncomingDto);
      } else {
        const studentIncomingDto: StudentIncomingDto = {
          name: name,
          classId: parseInt(classId),
        };
        response = await addStudentsByClassId(parseInt(classId), studentIncomingDto);
      }
      if (classId == undefined) {
        navigate('/students');
      } else {
        navigate(`/classes/${classId}/students`);
      }
  } 

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Add new Student</Typography>
        <TextField
          id="studentName-textField"
          label="Name"
          variant="outlined"
          value={name}
          onChange={handleChangeName}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        { classId == undefined && <TextField
          id="class-input"
          label="Class"
          type="number"
          value={schoolClassId}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChangeClassId}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        /> }
        <Button variant="contained" onClick={handleAddStudent}>
          Add Student
        </Button>
      </Box>
    </div>
  );
}

export default AddCurrency;
