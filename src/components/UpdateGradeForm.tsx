import { TextField, Typography, Button, Box, CssBaseline } from "@mui/material";
import { ChangeEvent, useEffect, useState } from "react";
import Menu from "./Menu";
import { DatePicker } from "@mui/x-date-pickers";
import { addGrade, getGradeById, updateGrade } from "../api/gradeApi";
import { useNavigate, useParams } from "react-router-dom";
import { Grade, GradeIncomingDto } from "../types/gradeType";
import { addGradesByStudentId } from "../api/studentApi";

function UpdateGradeForm() {
  const { gradeId } = useParams();
  const navigate = useNavigate();
  const [grade, setGrade] = useState(0);
  const [notes, setNotes] = useState("");
  const [subject, setSubject] = useState("");
  const [classstudentId, setClassStudentId] = useState(0);
  const [date, setDate] = useState(new Date());
  const [type, setType] = useState("");

  const setInitialData = async () => {
    if (gradeId != undefined) {
        const response = await getGradeById(parseInt(gradeId));
        setGrade(response.grade);
        setNotes(response.notes);
        setSubject(response.subject);
        setClassStudentId(response.studentId);
        setDate(response.date);
        setType(response.type);
    }
  };

  useEffect(() => {
    setInitialData();
  }, []);


  const handleChangeGrade = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setGrade(parseInt(event.target.value));
  };

  const handleChangeNotes = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setNotes(event.target.value);
  };

  const handleChangeSubject = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setSubject(event.target.value);
  };

  const handleChangeStudentId = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setClassStudentId(parseInt(event.target.value));
  };

  const handleChangeType = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setType(event.target.value);
  };

  const handleChangeDate = (
    event: any
  ) => {
    setDate(event);
  };

  const handleUpdateGrade = async () => {
    if (gradeId != undefined) {
        const updatedGrade: Grade = {
            id: parseInt(gradeId),
            grade: grade,
            notes: notes,
            subject: subject,
            studentId: classstudentId,
            date: date,
            type: type,
        };
        let response = await updateGrade(parseInt(gradeId), updatedGrade);
    }
    navigate('/grades');
  };

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Update Grade</Typography>
        <TextField
          id="grade-subject-textField"
          label="Subject"
          variant="outlined"
          value={subject}
          onChange={handleChangeSubject}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="grade-grade-textField"
          label="Grade"
          type="number"
          value={grade}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChangeGrade}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="grade-notes-textField"
          label="Notes"
          variant="outlined"
          value={notes}
          onChange={handleChangeNotes}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <TextField
          id="grade-StudentId-textField"
          label="StudentId"
          variant="outlined"
          value={classstudentId}
          onChange={handleChangeStudentId}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <DatePicker label="Date" onChange={handleChangeDate} sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}/>
        <TextField
          id="grade-type-TextField"
          label="Type"
          type="text"
          onChange={handleChangeType}
          InputLabelProps={{
            shrink: true,
          }}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
          value={type}
        />
        <Button variant="contained" onClick={handleUpdateGrade}>
          Update
        </Button>
      </Box>
    </div>
  );
}

export default UpdateGradeForm;
