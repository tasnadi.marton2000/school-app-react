import { TextField, Button, Box, CssBaseline, Typography } from "@mui/material";
import { ChangeEvent, useEffect, useState } from "react";
import Menu from "./Menu";
import { useNavigate, useParams } from "react-router-dom";
import { getStudentById, updateStudent } from "../api/studentApi";
import { Student } from "../types/studentType";

function AddCurrency() {
  const navigate = useNavigate();
  const { studentId } = useParams();
  const [name, setName] = useState("");
  const [schoolClassId, setSchoolClassId] = useState(0);

  const setInitialData = async () => {
    if (studentId != undefined) {
        const response: Student = await getStudentById(parseInt(studentId));
        console.log(response);
        setName(response.name);
        setSchoolClassId(response.classId);
    }
  };

  useEffect(() => {
    setInitialData();
  }, []);

  const handleChangeName = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setName(event.target.value);
  };

  const handleChangeClassId = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setSchoolClassId(parseInt(event.target.value));
  };

  const handleUpdateStudent = async () => {
    if (studentId != undefined) {
        const student: Student = {
            id: parseInt(studentId),
            name: name,
            classId: schoolClassId,
        };
        const response = await updateStudent(parseInt(studentId), student);
      }
      navigate('/students');
  }

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Update Student</Typography>
        <TextField
          id="studentName-textField"
          label="Name"
          variant="outlined"
          value={name}
          onChange={handleChangeName}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <TextField
          id="class-input"
          label="Class"
          type="number"
          value={schoolClassId}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChangeClassId}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <Button variant="contained" onClick={handleUpdateStudent}>
          Update
        </Button>
      </Box>
    </div>
  );
}

export default AddCurrency;
