import { TextField, Typography, Button, Box, CssBaseline } from "@mui/material";
import { ChangeEvent, useState } from "react";
import Menu from "./Menu";
import { ClassIncomingDto } from "../types/classTypes";
import { addClass } from "../api/classApi";
import { useNavigate, useParams } from "react-router-dom";

function AddClassForm() {
  const navigate=useNavigate();
  const [nrClass, setNrClass] = useState(0);
  const [abbreviation, setAbbreviation] = useState("");
  const [profile, setProfile] = useState("");

  const handleChangeAbbreviation = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setAbbreviation(event.target.value);
  };

  const handleChangeProfile = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setProfile(event.target.value);
  };

  const handleChangeNrClass = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setNrClass(parseInt(event.target.value));
  };

  const handleAddClass = async () => {
    const classIncomingDto: ClassIncomingDto = {
        nrClass: nrClass,
        abbreviation: abbreviation,
        profile: profile,
    };

    const response = await addClass(classIncomingDto);

    navigate('/classes');
  };

  return (
    <div style={{width: 500 }}>
      <CssBaseline />
      <Menu />
      <Box
        sx={{
          margin: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 1,
          boxShadow: 5,
          p: 3,
          width: "100%",
        }}
      >
        <Typography variant="h6">Add new Class</Typography>
        <TextField
          id="class-nrClass-textField"
          label="Class"
          variant="outlined"
          type="number"
          onChange={handleChangeNrClass}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="class-abbreviation-textField"
          label="Abbreviation"
          variant="outlined"
          onChange={handleChangeAbbreviation}
          sx={{ marginTop: 2, width: "75%" }}
        />
        <TextField
          id="class-profile-textField"
          label="Profile"
          variant="outlined"
          onChange={handleChangeProfile}
          sx={{ marginTop: 2, marginBottom: 2, width: "75%" }}
        />
        <Button variant="contained" onClick={handleAddClass}>
          Add Class
        </Button>
      </Box>
    </div>
  );
}

export default AddClassForm;
