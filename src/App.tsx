import { Routes, Route } from 'react-router-dom'
import './App.css'
import ClassesTable from './components/ClassesTable'
import AddClassForm from './components/AddClassForm';
import StudentsTable from './components/StudentsTable';
import AddStudentForm from './components/AddStudentForm';
import GradesTable from './components/GradesTable';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import AddGradeForm from './components/AddGradeForm';
import UpdateClassForm from './components/UpdateClassForm';
import UpdateStudentForm from './components/UpdateStudentForm';
import UpdateGradeForm from './components/UpdateGradeForm';
import SearchGrades from './components/SearchGrades';

function App() {

  return (
    <>
    <LocalizationProvider dateAdapter={AdapterDayjs}>
     <Routes>
        <Route path="/" element={<ClassesTable />} />
        <Route path="/classes" element={<ClassesTable />} />
        <Route path="/classes/:classId/students" element={<StudentsTable />} />
        <Route path="/classes/:classId/addStudent" element={<AddStudentForm />} />
        <Route path="/classes/:classId/update" element={<UpdateClassForm />} />
        <Route path="/addClass" element={<AddClassForm />} />
        <Route path="/students" element={<StudentsTable />} />
        <Route path="/students/:studentId/addGrade" element={<AddGradeForm />} />
        <Route path="/students/:studentId/grades" element={<GradesTable />} />
        <Route path="/students/:studentId/update" element={<UpdateStudentForm />} />
        <Route path="/addStudent" element={<AddStudentForm />} />
        <Route path="/grades" element={<GradesTable />} />
        <Route path="/grades/:gradeId/update" element={<UpdateGradeForm />} />
        <Route path="/addGrade" element={<AddGradeForm />} />
        <Route path="/search" element={<SearchGrades />} />
      </Routes>
      </LocalizationProvider>
    </>
  )
}

export default App;
