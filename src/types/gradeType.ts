export type Grade = {
    id: number;
    grade: number;
    notes: string;
    subject: string;
    studentId: number;
    date: Date;
    type: string;
  }

  export type GradeIncomingDto = {
    grade: number;
    notes: string;
    subject: string;
    studentId: number;
    date: Date;
    type: string;
  }