export type Class = {
    id: number;
    nrClass: number;
    abbreviation: string;
    profile: string;
  }

  export type ClassIncomingDto = {
    nrClass: number;
    abbreviation: string;
    profile: string;
  }