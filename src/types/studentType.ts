export type Student = {
    id: number;
    name: string;
    class?: number;
    classId: number;
  }

  export type StudentIncomingDto = {
    name: string;
    classId: number;
  }